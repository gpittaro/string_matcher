# Sparse Array manipulation API

This is an API that lets you query a string of comma separated words and
compare it to the words defined internally as an environment variable.

It can return the number of times each word from the query is found
in the environment variable.

Note: The string against which the API executes the query can be
modified by changing the `STRINGS` environment variable in
the `Dockerfile`.

The API management can be found in the `main.py` file.

`sparse_array.py` contains the SparseArray class with the logic to compare strings.
The class solves the following problem:
https://www.hackerrank.com/challenges/sparse-arrays/problem

The API is ran through Docker.
`Dockerfile` sets the python3.7 environment and imports the dependencies
defined in the `requirements.txt` file (flask related imports), sets the 
environment variables and launches the API script.

The API is documented with swagger.

## Install

    git clone https://gitlab.com/gpittaro/string_matcher.git

## Build the docker image

    docker build . -t string_matcher_api

## Run the API

    docker run -p 5000:5000 string_matcher_api

# REST API

The REST API and example request are described below.

## Get dict of counts of queries' words found

### Request

`GET /{queries}`

    curl -X GET "http://0.0.0.0:5000/ab,abc,bc" -H  "accept: application/json"

### Request URL

    http://0.0.0.0:5000/ab,abc,bc

### Response

     content-length: 43
     content-type: application/json
     date: Sun, 21 Jun 2020 17:34:50 GMT
     server: Werkzeug/0.16.0 Python/3.7.7 

     {
     	"ab": 2,
     	"abc": 1,
     	"bc": 0
     }
