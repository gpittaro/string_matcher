class SparseArray(object):
    """Class to make operations on arrays.
    
    Attributes:
        string_list (list of str): A list of strings 
            against which we perform operations.
        
    Args:
        string (str): Comma separated words used
            to create the string_list attribute.
            
    """
    
    def __init__(self, strings):
        """>>> test_class = SparseArray('ab,abc,bc')
        >>> test_class.string_list
        ['ab', 'abc', 'bc']
        
        """
        self.string_list = strings.split(',')
        
    def match_strings(self, queries):
        """For each query substring, determine how many times it 
        occurs in the `string_list` of the class.
        
        Args:
            queries (str): Comma separated words to find in 
                the class attribute `string_list`.
        
        Returns:
            list of int: Number of time each element from
                the queries are found in `string_list`.
                
        Examples :
        
        >>> test_class = SparseArray('ab,abc,bc,ab')
        >>> test_class.match_strings('ab,bc,bd')
        [2, 1, 0]
        
        """
        query_list = queries.split(',')
        found_words = []
        for queried_word in query_list:
            found_words.append(self.string_list.count(queried_word))
        return(found_words)
        
if __name__ == '__main__':
    import doctest
    doctest.testmod()