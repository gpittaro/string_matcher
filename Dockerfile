FROM python:3.7

COPY . /app

WORKDIR /app

RUN pip3 install -r requirements.txt

ENV STRINGS=ab,ab,abc
ENV FLASK_APP=main.py
ENV FLASK_ENV=development

ENTRYPOINT ["python3","main.py"]