from flask import Flask
from flask_restplus import Api, Resource

app = Flask(__name__)
api = Api(app)

name_space = api.namespace('/', description='Main APIs for array manipulation')

@name_space.route('/<string:queries>')
class ArrayManipulation(Resource):
    def get(self,queries):
        """Finds the number of time each word from the query can be found in the env string.

        Reads a string of comma separated words from
        the standard input and prints the number of times
        each one is found in the environement variable `strings`.
        
        The results is printed in the form of a `dict {str : int}`
        with the key being the substring from the argument passed
        in command line and the value being the number of times
        it has been found in the environment variable.
        """
        from sparse_array import SparseArray
        import sys
        import os
        
        strings = os.environ['STRINGS']
        
        matcher = SparseArray(strings)
        number_matches = matcher.match_strings(queries)
        #Turns the query list and the result list in one dict
        dict_matches = dict(zip(queries.split(','), number_matches))
        return(dict_matches)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')